public class GasContainer extends Item {

    private int volume;

    public GasContainer(final int id, final Point point, final int length, final int width, final int angle, final int volume) {
        super(id, point, length, width, angle);
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }

}
