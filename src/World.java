import java.util.List;

public class World {

    private final List<SpaceShip> spaceShips;
    private final List<GasContainer> gasContainers;
    private final Arena arena;
    private final SpaceShip playerSpaceShip;

    public World(final List<SpaceShip> spaceShips, final List<GasContainer> gasContainers,
                 final Arena arena, final SpaceShip playerSpaceShip) {
        this.spaceShips = spaceShips;
        this.gasContainers = gasContainers;
        this.arena = arena;
        this.playerSpaceShip = playerSpaceShip;
    }

    public List<SpaceShip> getSpaceShips() {
        return spaceShips;
    }

    public List<GasContainer> getGasContainers() {
        return gasContainers;
    }

    public Arena getArena() {
        return arena;
    }

    public SpaceShip getPlayerSpaceShip() {
        return playerSpaceShip;
    }

}
