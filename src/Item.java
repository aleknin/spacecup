

public class Item {

    private final Point point;
    private final int id;
    private final int length;
    private final int width;
    private final int angle;

    public Item(final int id, final Point point, final int length, final int width, final int angle) {
        this.point = point;
        this.id = id;
        this.length = length;
        this.width = width;
        this.angle = angle;
    }

    public final Point getPoint() {
        return point;
    }

    public final int getId() {
        return id;
    }

    public final int getLength() {
        return length;
    }

    public final int getWidth() {
        return width;
    }

    public final int getAngle() {
        return angle;
    }
}