public class SpaceShip extends Item {

    private final int velocity;

    public SpaceShip(final int id, final Point point, final int length,
                     final int width, final int angle, final int velocity) {
        super(id, point, length, width, angle);
        this.velocity = velocity;
    }

    public int getVelocity() {
        return velocity;
    }
}
