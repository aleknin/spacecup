public class Move {

    private boolean emit;
    private boolean fire;
    private byte turn;

    public void setEmit(final boolean emit) {
        this.emit = emit;
    }

    public void setFire(final boolean fire) {
        this.fire = fire;
    }

    public void setTurn(final byte turn) {
        this.turn = turn;
    }

}
